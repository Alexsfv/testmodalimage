import React from 'react';
import './App.scss';
import ImagesBlock from './Components/ImagesBlock/ImagesBlock';

function App() {
  return (
    <>
      <h1 style={{textAlign: 'center'}}>Пример галереи для просмотра макетов</h1>
      <ImagesBlock />
    </>
  );
}

export default App;
