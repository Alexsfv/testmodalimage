export type IImageData = {
    id: number,
    src: string,
    title: string
}

export type IImagesData = Array<IImageData>

export type IImgStyles = {
    maxWidth: string
    maxHeight: string
    width: string
    height: string
}