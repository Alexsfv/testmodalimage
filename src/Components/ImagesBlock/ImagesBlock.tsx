import React, { useState } from 'react'
import ImageItem from '../ImageItem/ImageItem'
import './ImagesBlock.scss'
import { IImageData, IImagesData } from '../../types/types'
import Gallery from '../Gallery/Gallery'
import { CSSTransition } from 'react-transition-group'
import { lockBody, unlockBody } from '../../functions/functions'

type ImagesBlockProps = {}
const ImagesBlock: React.FC<ImagesBlockProps> = () => {

    const initialImagesData: IImagesData = [
        {
            id: 1,
            src: 'https://ds02.infourok.ru/uploads/ex/1038/00068c5f-b17b6365/hello_html_m1a72d83b.png',
            title: 'Горизонтальная картинка большого размера'
        },
        {
            id: 2,
            src: 'https://www.hdwallpapers.net/wallpapers/first-lake-alberta-canada-wallpaper-for-nexus-7-104-251.jpg',
            title: 'Вертикальная картинка большого размера'
        },
        {
            id: 3,
            src: 'https://via.placeholder.com/2203',
            title: 'Work 3'
        },
        {
            id: 4,
            src: 'https://via.placeholder.com/254',
            title: 'Work 4'
        },
        {
            id: 5,
            src: 'https://via.placeholder.com/305',
            title: 'Work 5'
        },        
        {
            id: 6,
            src: 'https://via.placeholder.com/356',
            title: 'Work 6'
        },
        {
            id: 7,
            src: 'https://via.placeholder.com/357',
            title: 'Work 7'
        },
        {
            id: 8,
            src: 'https://via.placeholder.com/358',
            title: 'Work 8'
        },
    ]

    const [imagesData] = useState<IImagesData>(initialImagesData)
    const [showGallery, setShowGallery] = useState<boolean>(false)
    const [currentImageData, setCurrentImageData] = useState<IImageData>({} as IImageData)

    function clickImageItemHandler(e: React.MouseEvent<HTMLDivElement>, imageData: IImageData): void {
        setCurrentImageData(imageData)
        setShowGallery(true)
        lockBody()
    }

    const closeGallery = () => {
        setShowGallery(false)
        unlockBody()
    }

    return (
        <>
            <div className="images-block">
                <div className="container-images-block">
                    <div className="images-block__line">

                        {
                            imagesData.map(imageData => {

                                return (
                                    <ImageItem
                                        imageData={imageData}
                                        onClick={clickImageItemHandler}
                                        key={imageData.id}
                                    />
                                )
                            })
                        }

                    </div>
                </div>
            </div>

            <CSSTransition
                    in={showGallery}
                    timeout={600}
                    classNames={{
                        enter: 'gallery__enter',
                        enterActive: 'gallery__enter',
                        enterDone: 'gallery__enter',
                        exit: 'gallery__exit',
                        exitActive: 'gallery__exit',
                        exitDone: 'gallery__exit'
                    }}
                    unmountOnExit
            >
                <Gallery
                    currentImageData={currentImageData}
                    onClose={closeGallery}
                />
            </CSSTransition>
        </>
    )
}

export default ImagesBlock