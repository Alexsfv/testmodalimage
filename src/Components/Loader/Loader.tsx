import React from 'react'
import './Loader.scss'

type LoaderProps = {
    customClass?: string
}

const Loader: React.FC<LoaderProps> = ({customClass}) => {

    return (
        <div className={customClass}>
            <div className="loader-ellipsis">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}

export default Loader