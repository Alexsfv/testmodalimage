import React, { useState } from 'react'
import { IImageData } from '../../types/types'
import 'react-input-range/lib/css/index.css'
import './Gallery.scss'
import GalleryImageField from './GalleryImageField/GalleryImageField'
import GallerySidebar from './GallerySidebar/GallerySidebar'

type GalleryProps = {
    currentImageData: IImageData
    onClose: () => void
}
const Gallery: React.FC<GalleryProps> = ({currentImageData, onClose}) => {

    const [showInfoResponse, setShowInfoResponse] = useState<boolean>(false)
    const [isFullsize, setFullsize] = useState<boolean>(false)
    const [resetZoom] = useState({resetCallback: () => {}})

    const closeHandler = () => {
        onClose()
        resetZoom.resetCallback()
        setFullsize(false)
    }

    const toggleInfoResponseHandler = () => {
        setShowInfoResponse(!showInfoResponse)
    }

    const closeInfoResponseHandler = () => {
        setShowInfoResponse(false)
    }

    const toggleFullSize = () => {
        setFullsize(!isFullsize)
        if (isFullsize) {
            resetZoom.resetCallback()
        }
    }

    const minusFullscreenHandler = () => {
        setFullsize(false)
        resetZoom.resetCallback()
    }

    const galleryBodyClasses = ['gallery__body']

    if (isFullsize) {
        galleryBodyClasses.push('fullsize-md')
    }

    return (
        <div className="gallery">
            <div className="gallery__overlay">
                <div className="container-gallery">
                

                    <div className="gallery__close-btn" onClick={closeHandler}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </div>

                    <div className={galleryBodyClasses.join(' ')}>
                        
                        <GalleryImageField 
                            isFullsize={isFullsize}
                            currentImageData={currentImageData}
                            resetZoom={resetZoom}
                            toggleInfoResponseHandler={toggleInfoResponseHandler}
                            minusFullscreenHandler={minusFullscreenHandler}
                        />

                       <GallerySidebar
                            isFullsize={isFullsize}
                            showInfoResponse={showInfoResponse}
                            currentImageData={currentImageData}
                            closeInfoResponseHandler={closeInfoResponseHandler}
                            toggleFullSize={toggleFullSize}
                       />                        
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Gallery