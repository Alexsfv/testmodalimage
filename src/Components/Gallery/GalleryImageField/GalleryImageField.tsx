import React, { useEffect, useRef, useState } from 'react'
import InputRange from 'react-input-range'
import { IImageData, IImgStyles } from '../../../types/types'
import Loader from '../../Loader/Loader'

type GalleryImageFieldProps = {
    isFullsize: boolean
    currentImageData: IImageData
    resetZoom: { resetCallback: () => void }
    toggleInfoResponseHandler: () => void
    minusFullscreenHandler: () => void
}

const GalleryImageField: React.FC<GalleryImageFieldProps> = ({isFullsize, currentImageData, resetZoom, toggleInfoResponseHandler, minusFullscreenHandler}) => {

    let startPositionMouseX = 0
    let startPositionMouseY = 0

    const [isLoadedImg, setLoadedImg] = useState<boolean>(false)
    const [imgStyles, setImgStyles] = useState<IImgStyles>({maxWidth: '', maxHeight: '', width: '', height: ''})
    const [rangeValue, setRangeValue] = useState(10)
    const [imgMaxWidth, setImgMaxWidth] = useState(-1)
    const [imgMaxHeight, setImgMaxHeight] = useState(-1)

    const imgContainerRef = useRef<HTMLDivElement>(null)
    const imgRef = useRef<HTMLImageElement>(null)

    
    useEffect(() => {
        function resizeHandler() {
            setZoomDefault()
        }
        window.addEventListener('resize', resizeHandler)
        return () => {
            window.removeEventListener('resize', resizeHandler)
            setZoomDefault()
        }
    }, [])

    const imageMouseDownHandler = (e: React.MouseEvent<HTMLDivElement>) => {
        e.preventDefault()
        if (isFullsize) {
            startImageMoving(e)
        }
    }

    const startImageMoving = (e: React.MouseEvent<HTMLDivElement>) => {
        startPositionMouseX = e.clientX
        startPositionMouseY = e.clientY
        window.addEventListener('mousemove', moveImage)
    }

    const stopImageMoving = () => {
        window.removeEventListener('mousemove', moveImage)
    }

    const moveImage = (e: MouseEvent) => {
        const currentMousePositionX = e.clientX
        const currentMousePositionY = e.clientY

        const deltaX = currentMousePositionX - startPositionMouseX
        const deltaY = currentMousePositionY - startPositionMouseY

        const scrollField = imgContainerRef.current as HTMLDivElement
        scrollField.scrollLeft += 15 * Math.sign(deltaX)
        scrollField.scrollTop += 15 * Math.sign(deltaY) 

        startPositionMouseX = currentMousePositionX
        startPositionMouseY = currentMousePositionY
    }

    const setZoomDefault = () => {
        setImgStyles({maxWidth: '', maxHeight: '', width: '', height: ''})
        setRangeValue(10)
    }

    resetZoom.resetCallback = setZoomDefault

    const changeRangeHandler = (value: number) => {
        let pureWidthImg
        let pureHeightImg
        let fixedValue = value / 10
        const imgWidth = imgRef.current?.clientWidth as number
        const imgHeight = imgRef.current?.clientHeight as number
        const containerWidth = imgContainerRef.current?.clientWidth as number
        const containerHeight = imgContainerRef.current?.clientHeight as number

        if (imgMaxWidth === -1) {
            setImgMaxWidth(imgWidth)
            pureWidthImg = imgWidth           
        } else {
            pureWidthImg = imgMaxWidth
        }

        if (imgMaxHeight === -1) {
            setImgMaxHeight(imgHeight)
            pureHeightImg = imgHeight
        } else {
            pureHeightImg = imgMaxHeight
        }

        const percentX = (pureWidthImg / containerWidth) * 100 * fixedValue
        const percentY = (pureHeightImg / containerHeight) * 100 * fixedValue

        setImgStyles({
            maxWidth: 'none',
            maxHeight: 'none',
            width: percentX + '%',
            height: percentY + '%'
        })

        setRangeValue(value as number)
    }

    const imageFieldClasses = ['gallery__image-field']
    const imageClasses = ['gallery__image']
    const imgClasses = ['']
    const infoBtnClasses = ['gallery__info-btn']
    const minusFullscreenBtnClasses = ['gallery__minus-fullscreen-btn']

    if (isFullsize) {
        imageFieldClasses.push('fullsize-md')
        imageClasses.push('fullsize')
        minusFullscreenBtnClasses.push('fullsize')
        infoBtnClasses.push('fullsize')
    }

    if (!isLoadedImg) {
        imgClasses.push('loading')
    }

    return (   
        <>
    
            <button className={infoBtnClasses.join(' ')} onClick={toggleInfoResponseHandler}>
                <i className="fa fa-info-circle" aria-hidden="true"></i>
            </button>

            <button className={minusFullscreenBtnClasses.join(' ')} onClick={minusFullscreenHandler}>
                <i className="fa fa-search-minus" aria-hidden="true"></i>
            </button>


            <div className={imageFieldClasses.join(' ')}>

                <div 
                    className={imageClasses.join(' ')} 
                    onMouseDown={imageMouseDownHandler} 
                    ref={imgContainerRef}
                    onMouseUp={stopImageMoving}
                    onMouseLeave={stopImageMoving}
                >
                    <img 
                        src={currentImageData.src} 
                        alt="prototypeImage" 
                        ref={imgRef}
                        onLoad={() => setLoadedImg(true)}
                        className={imgClasses.join(' ')}
                        style={imgStyles}
                    />
                </div>

                {
                    isLoadedImg ? null : <Loader customClass="gallery__img-loader"/>
                }

                {
                    isFullsize 
                        ?   <InputRange
                                maxValue={20}
                                minValue={0}
                                value={rangeValue}
                                step={1}
                                onChange={value => changeRangeHandler(value as number)}
                                formatLabel={value => `${(value/10).toFixed(1)}x`}
                            />
                        : null
                }
            </div>
        </>
    )
}

export default GalleryImageField