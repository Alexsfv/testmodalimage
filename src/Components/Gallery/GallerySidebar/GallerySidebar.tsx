import React from 'react'
import { IImageData } from '../../../types/types'

type GallerySidebarProps = {
    isFullsize: boolean
    showInfoResponse: boolean
    currentImageData: IImageData
    closeInfoResponseHandler: () => void
    toggleFullSize: () => void
}
const GallerySidebar:React.FC<GallerySidebarProps> = ({isFullsize, showInfoResponse, currentImageData, closeInfoResponseHandler, toggleFullSize}) => {

    const sidebarClasses = ['gallery__sidebar']

    if (isFullsize) {
        sidebarClasses.push('fullsize-md')
    }

    if (showInfoResponse) {
        sidebarClasses.push('active')
    } 

    return (
        <div className={sidebarClasses.join(' ')}>
            <button className="gallery__close-sidebar" onClick={closeInfoResponseHandler}>
                <i className="fa fa-times-circle" aria-hidden="true"></i>
            </button>

            <div className="gallery__info-field">
                <p className="gallery__info-text">Информация о макете</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
                <p className="gallery__info-category">Название: {currentImageData.title}</p>
            </div>

            <div className="gallery__action-field">
                <button className="gallery__action-like">
                    <i className="fa fa-heart" aria-hidden="true"></i>
                </button>

                <button className="gallery__action-zoom" onClick={toggleFullSize}>
                    <i className="fa fa-search-plus" aria-hidden="true"></i>
                    <span>1:1</span>
                </button>

                <button className="gallery__action-order">Заказать</button>
            </div>
        </div>
    )
}

export default GallerySidebar