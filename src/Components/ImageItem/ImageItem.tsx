import React, { useState } from 'react'
import { IImageData } from '../../types/types'
import Loader from '../Loader/Loader'

type ImageProps = {
    imageData: IImageData
    onClick: (e: React.MouseEvent<HTMLDivElement>, imageData: IImageData) => void
}
const ImageItem: React.FC<ImageProps> = ({imageData, onClick}) => {

    const [isLoadedImg, setLoadedImg] = useState<boolean>(false)

    const clickHandler = (e: React.MouseEvent<HTMLDivElement>) => {
        onClick(e, imageData)
    }

    const imgClasses = ['images-block__image']
    if (!isLoadedImg) {
        imgClasses.push('loading')
    }

    return (
        <div className="images-block__item" onClick={clickHandler}>
            <div className={imgClasses.join(' ')}>
                <img 
                    src={imageData.src} 
                    alt="prototypeImage"
                    onLoad={() => setLoadedImg(true)}
                />

                {
                    isLoadedImg ? null : <Loader customClass="images-block__loader"/>
                }
            </div>

            <p className="images-block__item-title">{imageData.title}</p>
        </div>
    )
}

export default ImageItem